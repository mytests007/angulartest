﻿using AutoMapper;
using Moq;
using PolicyManagement.Contracts;
using PolicyManagement.Model.Data;
using PolicyManagement.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PolicyManagement.Service.Tests.Services
{
    public class PolicyServiceTests
    {
        private readonly Mock<IPolicyRepository> _mockedPolicyRepository;
        private readonly Mock<IMapper> _mockedMapper;
        private readonly PolicyService _mockedServiceInstance;

        public PolicyServiceTests()
        {
            _mockedPolicyRepository = new Mock<IPolicyRepository>();
            _mockedMapper = new Mock<IMapper>();
            _mockedServiceInstance = new PolicyService(_mockedPolicyRepository.Object, _mockedMapper.Object);
        }

        [Fact]
        public async Task GetAsync_WhenEmptyList_ReturnTotalZero()
        {
            _mockedPolicyRepository.Setup(x => x.GetAsync()).ReturnsAsync(new List<PolicyModel>().AsQueryable());

            var response = await _mockedServiceInstance.GetAsync(new FilterViewModel(null, null, null));

            Assert.Equal(0, response.Total);
        }

        [Fact]
        public async Task GetAsync_WhenFiltered_ReturnFilteredCount()
        {
            var mockedPolicies = new List<PolicyModel>()
            {
                new PolicyModel
                {
                    PolicyHolder = new UserModel
                    {
                        Name ="testName"
                    },
                    PolicyNumber = "mock1"
                },
                new PolicyModel
                {
                    PolicyHolder = new UserModel
                    {
                        Name ="mockName"
                    },
                    PolicyNumber = "test"
                },
                new PolicyModel
                {
                    PolicyHolder = new UserModel
                    {
                        Name ="testName"
                    },
                    PolicyNumber = "test"
                }
            };

            _mockedPolicyRepository.Setup(x => x.GetAsync()).ReturnsAsync(mockedPolicies.AsQueryable());
            _mockedMapper.Setup(x => x.Map<List<PolicyViewModel>>(It.IsAny<List<PolicyModel>>())).Returns(new List<PolicyViewModel>());
            var response = await _mockedServiceInstance.GetAsync(new FilterViewModel("mock", null, null));

            Assert.Equal(2, response.Total);
        }
    }
}
