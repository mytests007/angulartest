﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PolicyManagement.Common.Logger;
using PolicyManagement.Model.Config;
using PolicyManagement.Model.ViewModel;
using PolicyManagement.Service;

namespace PolicyManagement.Controllers
{
    [Route("api/token")]
    public class TokenController : Controller
    {
        private readonly IUserService _userService;
        private readonly ITokenProvider _tokenProvider;
        private readonly ILogger _logger;
        private readonly TokenOptions _tokenOptions;

        public TokenController(IUserService userService, ITokenProvider tokenProvider, 
            IOptions<TokenOptions>  tokenOptions, ILogger logger)
        {
            _userService = userService;
            _tokenProvider = tokenProvider;
            _logger = logger;
            _tokenOptions = tokenOptions.Value;
        }

        [Route("", Name = "GetToken")]
        [HttpGet]
        [ProducesResponseType(401)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get([FromQuery] string username, [FromQuery] string password)
        {
            if (!await _userService.AuthenticateUser(username, password))
            {
                _logger.Log($"User authentication failed for user {username}", Model.Logging.LogLevel.Info);
                return Unauthorized();
            }
                

            int ageInMinutes = _tokenOptions.AccessTokenExpiration;

            DateTime expiry = DateTime.UtcNow.AddMinutes(ageInMinutes);

            var token = new JsonWebToken
            {
                access_token = _tokenProvider.CreateToken(username, expiry),
                expires_in = ageInMinutes * 60
            };

            return Ok(token);
        }
    }
}