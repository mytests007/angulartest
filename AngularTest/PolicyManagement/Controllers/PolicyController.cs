using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PolicyManagement.Model.ViewModel;
using PolicyManagement.Service;

namespace PolicyManagement.Controllers
{
    [Authorize]
    [Route("api/policy")]
    public class PolicyController : Controller
    {
        private readonly IPolicyService _policyService;

        public PolicyController(IPolicyService policyService)
        {
            _policyService = policyService;
        }
        
        [Route("", Name = "GetAllPolicy")]
        [HttpGet]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get(string filter = null, int? pageSize =null, int? pageNumber = null)
        {
            var data = await _policyService.GetAsync(new FilterViewModel(filter, pageSize, pageNumber));
            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }

        [Route("{id}", Name = "GetPolicy")]
        [HttpGet]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest("Policy Id required");
            }

            var data = await _policyService.GetAsync(id.Value);

            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }

        [Route("", Name = "AddPolicy")]
        [HttpPost]
        [ProducesResponseType(422)]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Post([FromBody]PolicyViewModel model)
        {
            if (model == null)
            {
                return BadRequest("Policy details required");
            }

            var response = await _policyService.AddAsync(model);
            if (!response.HasValue)
            {
                return StatusCode(422, "Policy creation failed");
            }

            return Ok(response.Value);
        }

        [Route("{id}", Name = "UpdatePolicy")]
        [HttpPut]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Put(int? id, [FromBody]PolicyViewModel model)
        {
            if (!id.HasValue)
            {
                return BadRequest("Policy Id required");
            }

            if (model == null)
            {
                return BadRequest("Modified policy details required");
            }

            var response = await _policyService.UpdateAsync(id.Value, model);

            if (!response.HasValue)
            {
                return NotFound();
            }

            return Ok(response.Value);
        }

        [Route("{id}", Name = "DeletePolicy")]
        [HttpDelete]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest("Policy Id required");
            }

            var response = await _policyService.RemoveAsync(id.Value);

            if (!response.HasValue)
            {
                return NotFound();
            }

            return Ok(response.Value);
        }
    }
}
