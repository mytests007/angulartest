﻿using Microsoft.AspNetCore.Http;
using PolicyManagement.Common.Logger;
using PolicyManagement.Model.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyManagement.Middleware
{
    public class ErrorLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ErrorLoggingMiddleware(ILogger logger, RequestDelegate next)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.Log($"Exception: {ex.Message}. Stack Trace: {ex.StackTrace}", LogLevel.Error, ex);
                throw;
            }
        }
    }
}
