export class user {
    userName: string;
    password: string;
}

export class token {
    access_token: string;
}

export class policy{
    id: number;
    policyNumber: string;
    name: string;
    age: number;
    gender: number;
}

export class collectionResponse{
    total: number;
    items: any;
}