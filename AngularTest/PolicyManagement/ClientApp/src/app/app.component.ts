import { Component, OnInit } from '@angular/core';
import { SessionService } from './services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'ClientApp';
  constructor(private sessionService: SessionService, private router: Router){
  }

  ngOnInit(){
    var sessionId = this.sessionService.getSessionToken();
    if(!sessionId){
      this.router.navigateByUrl('/login');
    }
  }
}