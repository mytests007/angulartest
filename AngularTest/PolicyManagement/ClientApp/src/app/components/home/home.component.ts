import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../../services/policy/policy.service';
import { SessionService } from '../../services/session/session.service';
import { collectionResponse } from '../../models/model.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  policyList: any;
  p: number = 1;
  total: number;
  loading: boolean;
  pageSize: number = 10;
  searchValue: string;
  deletionEnabled: boolean = false;
  selectedId: number;
  noPolicyFound = false;
  serverActionFailed = false;
  errorMessage: string;
  constructor(private policyService: PolicyService, private sessionService: SessionService, private router: Router) { }

  ngOnInit() {
      this.getPage(1);
  }

  getPage(page: number) {
      this.loading = true;
      this.policyService.getAll(this.searchValue, this.pageSize, page)
      .subscribe((res: collectionResponse) => {
        if(res){
          this.total = res.total;
          this.p = page;
          this.loading = false;
          this.policyList = res.items
          this.noPolicyFound = false;
          
          if(this.total <= 0){
            this.noPolicyFound = true;
          }
        }
        else{
          this.noPolicyFound = true;
        }

        if(this.noPolicyFound){
          this.errorMessage = 'No policy found';
        }
      },
      error =>{
        this.noPolicyFound = true;
        this.errorMessage = 'No policy found';
          if(error && error.status == 401){
            this.sessionService.sessionOut();
          }
      });
  }

  doFilter(filter: string){
    this.searchValue = filter;
    this.getPage(this.p);
  }

  editPolicy(id){
    this.router.navigateByUrl('/editpolicy/'+ id);
  }

  deletePolicy(id){
    this.selectedId = id;
    this.deletionEnabled= true;
  }

  cancelDeletion(){
    this.selectedId = null;
    this.deletionEnabled= false;
  }

  onDeletion(){
    this.policyService.delete(this.selectedId)
    .subscribe((res) => {
      if(res){
        this.cancelDeletion();
        this.getPage(this.p);
      }
      else{
        this.serverActionFailed = true;
        this.errorMessage = 'Failed to delete policy';
      }
    },
    error =>{
      this.serverActionFailed = true;
      this.errorMessage = 'Failed to delete policy';
        if(error && error.status == 401){
          this.sessionService.sessionOut();
        }
    });
  }
}
