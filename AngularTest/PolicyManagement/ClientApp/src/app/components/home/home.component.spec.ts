import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { HeaderComponent }  from '../header/header.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { PolicyService } from '../../services/policy/policy.service';
import { SessionService } from '../../services/session/session.service';
import { RouterTestingModule } from '@angular/router/testing';
import {NgxPaginationModule} from 'ngx-pagination';
import { MockPolicyService } from '../../mock/policy.service.mock';
import { Router } from '@angular/router';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let policyService: PolicyService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent, HeaderComponent ],
      imports: [ FormsModule, ReactiveFormsModule, HttpModule, RouterTestingModule, NgxPaginationModule],
      providers:
               [{provide : PolicyService, useClass:  MockPolicyService},
                HttpClient, HttpHandler, SessionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    policyService = TestBed.get(PolicyService);
    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when getAll called, returns policyList', async(done)=>{
    var searchValue = '', pageSize = 10, page=1;

    component.getPage(page);

    let response = policyService.getAll(searchValue, pageSize, page);
    response.toPromise().then((response)=>{
        expect(response).toBeDefined();
        expect(component.total).toEqual(10);
        expect(component.noPolicyFound).toEqual(false);
    })
    done();
  },50);

  it('when deletepolicy called deletion screen enabled', () => {
    component.deletePolicy(1);

    expect(component.selectedId).toEqual(1);
    expect(component.deletionEnabled).toEqual(true);
  });

  it('when canceldeletion called deletion screen disabled', () => {
    component.cancelDeletion();

    expect(component.selectedId).toEqual(null);
    expect(component.deletionEnabled).toEqual(false);
  });

  it('when doFilter called searched with filter value', async(done)=>{
    var searchValue = 'test';

    spyOn(policyService, 'getAll').and.callThrough();
    
    component.doFilter(searchValue);
    expect(policyService.getAll).toHaveBeenCalledWith(searchValue, 10, 1);
    done();
  },50);

  it('when editPolicy called navigated to edit page', () => {

    spyOn(router, 'navigateByUrl').and.callThrough();

    component.editPolicy(1);
    
    expect(router.navigateByUrl).toHaveBeenCalledWith('/editpolicy/1');
  });

  
  it('when onDeletion called, deletes policy', async()=>{
    spyOn(policyService, 'delete').and.callThrough();
    spyOn(component, 'cancelDeletion').and.callThrough();
    spyOn(component, 'getPage').and.callThrough();
    component.selectedId = 2;

    component.onDeletion();

    expect(policyService.delete).toHaveBeenCalledWith(2);
    expect(component.cancelDeletion).toHaveBeenCalled();
    expect(component.getPage).toHaveBeenCalledWith(1);
  })
});
