import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session/session.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // header message
  headMessage = "My Insurance Portal";
  constructor(private sessionService: SessionService) { }

  ngOnInit() {
  }
  
  // signout method which clears session
  signout(){
    this.sessionService.sessionOut();;
  }
}
