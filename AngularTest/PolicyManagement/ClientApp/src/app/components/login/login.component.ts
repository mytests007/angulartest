import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Md5 } from 'ts-md5/dist/md5';
import { UserService } from '../../services/user/user.service'
import { SessionService } from '../../services/session/session.service';
import { Router } from '@angular/router';
import { user, token } from '../../models/model.interface';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

loginForm: FormGroup;
defaultLoginError = "user login failed";
loginError: string;
loginFailed = false;

constructor(private userService: UserService, private formBuilder:FormBuilder, 
            private sessionService: SessionService, private router: Router) {
  // configuring form controls validations
  this.loginForm =formBuilder.group({
    'userName': [null, Validators.compose([Validators.required, Validators.maxLength(20)])],
    'password': [null, Validators.required]
  })
 }

authenticateUser(user: user){
  // authentication service calls
   let hasedPassword: string = Md5.hashStr(user.password).toString();
   this.userService.authenticate(user.userName, hasedPassword)
   .subscribe(
     (data: token)=>{
       if(data){
         // setting session in local storage
         this.sessionService.sessionIn(data.access_token);
         this.sessionService.setSession("loginUser", user.userName);
         this.loginFailed = false;
         // navigating to home
         this.router.navigateByUrl('/home');
       }
       else{
         this.loginError = this.defaultLoginError;
         this.loginFailed = true;
       }
     },
     (error) => {
       this.loginError = this.defaultLoginError;
       this.loginFailed = true;
     }
   )
 }
}
