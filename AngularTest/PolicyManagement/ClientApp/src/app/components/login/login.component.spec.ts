import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from './login.component';
import { UserService } from '../../services/user/user.service';
import { SessionService } from '../../services/session/session.service';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { MockUserService } from '../../mock/user.service.mock';
import { user } from "../../models/model.interface";
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let userService: UserService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports:[   
              RouterModule.forRoot([
                { path: '', redirectTo: 'home', pathMatch: 'full' },
                {path: 'login', component: LoginComponent}
              ]),  
              FormsModule, ReactiveFormsModule, HttpModule],
      providers: [{provide: APP_BASE_HREF, useValue : '/' },
                  {provide : UserService, useClass:  MockUserService},
                   SessionService, HttpClient, HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    userService = TestBed.get(UserService);
    router = TestBed.get(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when authenticate called, gets token data', async(done)=>{
      var user: user = {userName: 'test', password: 'password'}

      component.authenticateUser(user);

      let response = userService.authenticate(user.userName, user.password);
      response.toPromise().then((response)=>{
          expect(response).toBeDefined();
          expect(component.loginFailed).toEqual(false);
          expect(router.navigateByUrl).toHaveBeenCalledWith('/home');
      })
      done();
  },50);

  it('when authenticate called, naviagated to home', async(done)=>{
    var user: user = {userName: 'test', password: 'password'}

    component.authenticateUser(user);

    let response = userService.authenticate(user.userName, user.password);
    response.toPromise().then(()=>{
        expect(router.navigateByUrl).toHaveBeenCalledWith('/home');
    })
    done();
  },50);

  it('when authenticate called, login success', async()=>{
    var user: user = {userName: 'test', password: 'password'}

    component.authenticateUser(user);
    
    expect(component.loginFailed).toEqual(false);
  },50);
});
