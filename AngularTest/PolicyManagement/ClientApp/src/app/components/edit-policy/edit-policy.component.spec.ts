import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent }  from '../header/header.component';
import { EditPolicyComponent } from './edit-policy.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { PolicyService } from '../../services/policy/policy.service'
import { SessionService } from '../../services/session/session.service';
import { RouterTestingModule } from '@angular/router/testing';
import { MockPolicyService } from '../../mock/policy.service.mock';
import { policy } from 'src/app/models/model.interface';

describe('EditPolicyComponent', () => {
  let component: EditPolicyComponent;
  let fixture: ComponentFixture<EditPolicyComponent>;
  let policyService: PolicyService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPolicyComponent, HeaderComponent ],
      imports: [
        FormsModule, ReactiveFormsModule, HttpModule, RouterTestingModule],
      providers:[{provide : PolicyService, useClass:  MockPolicyService}, 
                 HttpClient, HttpHandler, SessionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    policyService = TestBed.get(PolicyService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when getPolicy called, returns response', async(done)=>{
    component.policyId=1;

    component.getPolicy();
    let response = policyService.get(component.policyId);

    response.toPromise().then((response)=>{
        expect(response).toBeDefined();
    })
    done();
  },50);

  it('when getPolicy called, project object is set', async()=>{
    component.policyId=1;

    component.getPolicy();

    expect(component.policyObj.id).toEqual(1);
  });

  it('when getPolicy called, expect service method invoked', async()=>{
    component.policyId=1;

    spyOn(policyService, 'get').and.callThrough();
    component.getPolicy();

    expect(policyService.get).toHaveBeenCalledWith(1);
  });

  it('when onUpdate called, returns response', async(done)=>{
    component.policyId=1;
    var policy: policy;

    component.onUpdate(policy);
    let response = policyService.update(component.policyId, policy);

    response.toPromise().then((response)=>{
        expect(response).toBeDefined();
    })
    done();
  },50);

  it('when onUpdate called, expect service method invoked', async()=>{
    component.policyId=1;
    var policy: policy;

    spyOn(policyService, 'update').and.callThrough();
    component.onUpdate(policy);

    expect(policyService.update).toHaveBeenCalledWith(1, policy);
  });

  it('when onUpdate called, policy updated', async()=>{
    component.policyId=1;
    var policy: policy;

    component.onUpdate(policy);

    expect(component.policyUpdated).toEqual(true);
  });

  it('when onUpdate called, policy updated', async()=>{
    component.policyId=1;
    var policy: policy;

    component.onUpdate(policy);

    expect(component.policyUpdated).toEqual(true);
  });
});
