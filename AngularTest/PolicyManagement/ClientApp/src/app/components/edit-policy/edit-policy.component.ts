import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PolicyService } from '../../services/policy/policy.service'
import { SessionService } from '../../services/session/session.service';
import { maxValue } from '../../services/utility/utility.service';
import { policy } from '../../models/model.interface';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-edit-policy',
  templateUrl: './edit-policy.component.html',
  styleUrls: ['./edit-policy.component.css']
})
export class EditPolicyComponent implements OnInit {
  policyId : any;
  policyObj: policy;
  policyUpdated = false;
  policyForm: FormGroup;
  serverActionFailed = false;
  errorMessage: string;
  constructor(private activatedRoute: ActivatedRoute, private formBuilder:FormBuilder, 
    private sessionService: SessionService, private policyService: PolicyService) {
    this.policyObj = new policy();
    this.policyForm = this.formBuilder.group({
      'policyNumber': [null, Validators.compose([Validators.required, Validators.maxLength(15)])],
      'name': [null, Validators.compose([Validators.required, Validators.maxLength(20)])],
      'age': [null, Validators.compose([Validators.required, maxValue(99)])],
      'gender': [null, Validators.compose([Validators.required])]
    })
   }

  ngOnInit() {
        // subscribe to router event for identifying change in parameters in routing
        this.activatedRoute.params.subscribe((params: Params) => {
          // setting selected vedioId
          this.policyId = params['id'];
          // loading all videos
          this.getPolicy();
      });
  }

  getPolicy = ()=> {
    this.policyService.get(this.policyId)
    .subscribe((res : policy) => {
      if(res)
      {
        this.policyObj = res;
      }
      else{
        this.serverActionFailed = true;
        this.errorMessage = 'Failed to get policy';
      }
    },
    error =>{
      this.serverActionFailed = true;
      this.errorMessage = 'Failed to get policy';
        if(error && error.status == 401){
          this.sessionService.sessionOut();
        }
    });
  }

  onUpdate = (policy: policy)=> {
    this.policyService.update(this.policyId, policy)
    .subscribe(res => {
      if(res)
      {
        this.policyUpdated = true;
      }
      else{
        this.serverActionFailed = true;
        this.errorMessage = 'Failed to update policy';
      }
    },
    error =>{
      this.serverActionFailed = true;
      this.errorMessage = 'Failed to update policy';
        if(error && error.status == 401){
          this.sessionService.sessionOut();
        }
    });
  }


}
