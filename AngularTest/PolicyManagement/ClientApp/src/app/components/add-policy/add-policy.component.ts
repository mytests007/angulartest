import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PolicyService } from '../../services/policy/policy.service'
import { SessionService } from '../../services/session/session.service';
import { maxValue } from '../../services/utility/utility.service';
import { policy } from '../../models/model.interface';

@Component({
  selector: 'app-add-policy',
  templateUrl: './add-policy.component.html',
  styleUrls: ['./add-policy.component.css']
})
export class AddPolicyComponent implements OnInit {
  policyForm: FormGroup;
  policyAdded = false;
  serverActionFailed = false;
  errorMessage: string;
  constructor(private policyService: PolicyService, private formBuilder:FormBuilder, 
            private sessionService: SessionService) {
  // configuring form controls validations
  this.policyForm = this.formBuilder.group({
    'policyNumber': [null, Validators.compose([Validators.required, Validators.maxLength(15)])],
    'name': [null, Validators.compose([Validators.required, Validators.maxLength(20)])],
    'age': [null, Validators.compose([Validators.required, maxValue(99)])],
    'gender': [null, Validators.compose([Validators.required])]
  })
}

  ngOnInit() {
  }

  onSubmit = (policy: policy)=> {
    this.policyService.create(policy)
    .subscribe(res => {
      if(res)
      {
        this.policyAdded = true;
      }
      else{
        this.serverActionFailed = true;
        this.errorMessage = 'Failed to create policy';
      }
    },
    error =>{
      this.serverActionFailed = true;
      this.errorMessage = 'Failed to create policy';
        if(error && error.status == 401){
          this.sessionService.sessionOut();
        }
    });
  }

  onReload = () =>{
    this.policyForm.reset();
    this.policyAdded = false;
    this.serverActionFailed = false;
  }

}
