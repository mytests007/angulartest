import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent }  from '../header/header.component';
import { AddPolicyComponent } from './add-policy.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { PolicyService } from '../../services/policy/policy.service'
import { SessionService } from '../../services/session/session.service';
import { RouterTestingModule } from '@angular/router/testing';
import { MockPolicyService } from '../../mock/policy.service.mock';
import { policy } from 'src/app/models/model.interface';

describe('AddPolicyComponent', () => {
  let component: AddPolicyComponent;
  let fixture: ComponentFixture<AddPolicyComponent>;
  let policyService: PolicyService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPolicyComponent, HeaderComponent ],
      imports: [
         FormsModule,  
        ReactiveFormsModule, HttpModule, RouterTestingModule],
        providers:[{provide : PolicyService, useClass:  MockPolicyService},
           HttpClient, HttpHandler, SessionService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    policyService = TestBed.get(PolicyService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when onSubmit called, returns response', async(done)=>{
    var policy: policy;

    component.onSubmit(policy);
    let response = policyService.create(policy);

    response.toPromise().then((response)=>{
        expect(response).toBeDefined();
    })
    done();
  },50);

  it('when onSubmit called, expect service method invoked', async()=>{
    var policy: policy;

    spyOn(policyService, 'create').and.callThrough();
    component.onSubmit(policy);

    expect(policyService.create).toHaveBeenCalledWith(policy);
  });

  it('when onReload called, policyForm reset', async()=>{
    spyOn(component.policyForm, 'reset').and.callThrough();

    component.onReload();

    expect(component.policyForm.reset).toHaveBeenCalled();
  });

  it('when onReload called, variable reset', async()=>{
    component.onReload();

    expect(component.policyAdded).toEqual(false);
    expect(component.serverActionFailed).toEqual(false);
  });
});
