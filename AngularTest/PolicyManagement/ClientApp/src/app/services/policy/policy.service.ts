import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SessionService } from '../../services/session/session.service';

@Injectable({
  providedIn: 'root'
})
export class PolicyService {
  private apiRoute = "api/policy";
  constructor(private http: HttpClient, private sessionService: SessionService) {}
  
  public getAll = (filter: string, pageSize: number, page: number) => {
    var route = `?pageSize=${pageSize}&pageNumber=${page}`;
    if(filter){
      route = route + `&filter=${filter}`
    }

    return this.http.get(this.createCompleteRoute(route, environment.urlAddress), this.generateHeaders());
  }
 
  public get = (id: number) => {
    return this.http.get(this.createCompleteRoute(`/${id}`, environment.urlAddress), this.generateHeaders());
  }

  public create = (body) => {
    return this.http.post(this.createCompleteRoute('', environment.urlAddress), body, this.generateHeaders());
  }
 
  public update = (id: number, body) => {
    return this.http.put(this.createCompleteRoute(`/${id}`, environment.urlAddress), body, this.generateHeaders());
  }
 
  public delete = (id: number) => {
    return this.http.delete(this.createCompleteRoute(`/${id}`, environment.urlAddress), this.generateHeaders());
  }
 
  private createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${this.apiRoute}${route}`;
  }
 
  private generateHeaders = () => {
    var authenticationHeader = `Bearer ${this.sessionService.getSessionToken()}`;
    return {
      headers: new HttpHeaders({'Content-Type': 'application/json', 'Authorization': authenticationHeader})
    }
  }
}
