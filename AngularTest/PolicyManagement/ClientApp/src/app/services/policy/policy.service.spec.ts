import { TestBed } from '@angular/core/testing';
import { PolicyService } from './policy.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { SessionService } from '../session/session.service'
import { RouterTestingModule } from '@angular/router/testing';

describe('PolicyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [PolicyService, HttpClient, HttpHandler, SessionService]
    });
  });

  it('should be created', () => {
    const service: PolicyService = TestBed.get(PolicyService);
    expect(service).toBeTruthy();
  });
});
