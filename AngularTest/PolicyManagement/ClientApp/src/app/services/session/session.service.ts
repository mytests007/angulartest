import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class SessionService {
  private tokenKey: string = "loginUserSession";
  constructor(private router: Router){}

  setSession( key: string, value: string){
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem(key, JSON.stringify({
        "value": value
      }));
    } 
  }

  getSession(key: string){
    if (typeof(Storage) !== "undefined") {
      var jsonStr = localStorage.getItem(key);
      if(jsonStr){
        var jsonObj = JSON.parse(jsonStr);
        return jsonObj.value;
      }
    } 
  }

  sessionOut(){
    this.setSession(this.tokenKey, null);
    this.router.navigateByUrl('/login');
  }

  sessionIn(token: string){
    this.setSession(this.tokenKey, token);
  }

  getSessionToken(){
    return this.getSession(this.tokenKey);
  }
}
