import { TestBed, inject } from '@angular/core/testing';
import { SessionService } from './session.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SessionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [SessionService]
    });
  });

  it('session service initialised', inject([SessionService], (service: SessionService) => {
    expect(service).toBeTruthy();
  }));

  it('check saving and retrieving of session', inject([SessionService], (service: SessionService) => {
    let value = "Arun";
    service.setSession("keyTest", value)
    expect(service.getSession("keyTest")).toEqual(value);
  }));
});
