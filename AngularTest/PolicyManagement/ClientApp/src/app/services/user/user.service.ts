import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
 
@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private http: HttpClient) {}

  public authenticate = (userName: string, password: string) => {
    var route = `api/token?username=${userName}&password=${password}`;

    return this.http.get(this.createCompleteRoute(route, environment.urlAddress))
  }

  private createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${route}`;
  }
}