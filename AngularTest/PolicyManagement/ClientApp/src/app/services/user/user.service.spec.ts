import { TestBed } from '@angular/core/testing';
import { UserService } from './user.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { MockUserService } from "../../mock/user.service.mock";

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService, HttpClient, HttpHandler]
    });
  });


  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });
});

