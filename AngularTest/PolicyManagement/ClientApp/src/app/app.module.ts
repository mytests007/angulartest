import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {NgxPaginationModule} from 'ngx-pagination';

import { AppComponent } from "./app.component";
import { LoginComponent } from './components/login/login.component';
import { UserService } from './services/user/user.service';
import { SessionService } from './services/session/session.service';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { AddPolicyComponent } from './components/add-policy/add-policy.component';
import { EditPolicyComponent } from './components/edit-policy/edit-policy.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'addpolicy', component: AddPolicyComponent},
  {path: 'editpolicy/:id', component: EditPolicyComponent}
];

@NgModule({
  declarations: [AppComponent, LoginComponent, HomeComponent, HeaderComponent, AddPolicyComponent, EditPolicyComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,  
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [UserService, SessionService],
  bootstrap: [AppComponent]
})
export class AppModule {}