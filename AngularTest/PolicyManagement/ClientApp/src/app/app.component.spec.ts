import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {NgxPaginationModule} from 'ngx-pagination';
import {APP_BASE_HREF} from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { UserService } from './services/user/user.service';
import { SessionService } from './services/session/session.service';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { AddPolicyComponent } from './components/add-policy/add-policy.component';
import { EditPolicyComponent } from './components/edit-policy/edit-policy.component';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        HeaderComponent,
        HeaderComponent,
        AddPolicyComponent,
        EditPolicyComponent
      ],
       imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {path: 'login', component: LoginComponent},
      {path: 'home', component: HomeComponent},
      {path: 'addpolicy', component: AddPolicyComponent},
      {path: 'editpolicy/:id', component: EditPolicyComponent}
    ]),
    HttpClientModule,
    FormsModule,  
    ReactiveFormsModule,
    NgxPaginationModule],
     providers: [{provide: APP_BASE_HREF, useValue : '/' },
     SessionService, UserService]
    }).compileComponents();
  }));
  it('app component is up and running', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});