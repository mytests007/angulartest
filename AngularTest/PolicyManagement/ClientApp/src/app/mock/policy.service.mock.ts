import { collectionResponse, policy } from "../models/model.interface";
import { Observable, of } from "rxjs";

export class MockPolicyService{
    public getAll = (filter: string, pageSize: number, page: number) : Observable<collectionResponse> => {
        return of({
            total: 10,
            items: [{test: ''}]
        });
    }

    public get = (id: number) : Observable<policy> => {
        return of({
            id: 1,
            policyNumber: 'mockPolicy',
            name: 'mockName',
            age: 12,
            gender: 0
        });
    }
    
    public create = (body: policy) : Observable<number> => {
        return of(1)
    }
    
    public update = (id: number, body) : Observable<boolean> => {
        return of(true)
    }
    
    public delete = (id: number) : Observable<boolean> => {
        return of(true)
    }
}
