import { token } from "../models/model.interface";
import { Observable, of } from "rxjs";

export class MockUserService{
    public authenticate = (userName: string, password: string) : Observable<token> => {
        return of({
            access_token: 'idSuccess',
        });
      }
}
