﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using PolicyManagement.Data.Context;
using StructureMap;

namespace PolicyManagement.Registries
{
    public class PolicyRegistry : Registry
    {
        private readonly IConfigurationRoot _configuration;

        public PolicyRegistry(IConfigurationRoot configuration)
        {
            _configuration = configuration;

            Scan(_ =>
            {
                _.AssemblyContainingType<Startup>();
                _.Assembly("PolicyManagement.Service");
                _.Assembly("PolicyManagement.Contract");
                _.Assembly("PolicyManagement.Data");
                _.Assembly("PolicyManagement.Model");
                _.Assembly("PolicyManagement.Common");
                _.LookForRegistries();
                _.AddAllTypesOf<Profile>();
                _.WithDefaultConventions();
            });

            For<IDbContext>().Singleton().Use<DbContext>();
            For<IMapper>().Use(() => Mapper.Instance);
        }
    }
}