﻿using System.Threading.Tasks;

namespace PolicyManagement.Service
{
    public interface IUserService
    {
        Task<bool> AuthenticateUser(string userName, string password);
    }
}