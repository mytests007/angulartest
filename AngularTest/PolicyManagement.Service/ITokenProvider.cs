﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Service
{
    public interface ITokenProvider
    {
        string CreateToken(string userName, DateTime expiry);

        TokenValidationParameters GetValidationParameters();
    }
}
