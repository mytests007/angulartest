﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PolicyManagement.Model.ViewModel;

namespace PolicyManagement.Service
{
    public interface IPolicyService
    {
        Task<int?> AddAsync(PolicyViewModel newPolicy);

        Task<CollectionResponse<PolicyViewModel>> GetAsync(FilterViewModel filterModel);

        Task<PolicyViewModel> GetAsync(int id);

        Task<bool?> RemoveAsync(int id);

        Task<bool?> UpdateAsync(int id, PolicyViewModel policyVM);
    }
}