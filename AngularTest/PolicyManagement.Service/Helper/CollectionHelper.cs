﻿using PolicyManagement.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolicyManagement.Service.Helper
{
    public static class CollectionHelper
    {
        public static IQueryable<T> Paginate<T>(FilterViewModel filterModel, IQueryable<T> list)
        {
            if (filterModel.PageSize.HasValue)
            {
                var pageNumber = filterModel.PageNumber ?? 1;
                var skipValue = filterModel.PageSize.Value * (pageNumber - 1);
                list = list.Skip(skipValue).Take(filterModel.PageSize.Value);
            }

            return list;
        }
    }
}
