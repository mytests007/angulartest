﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace PolicyManagement.Service.Helper
{
    public static class EncrptionHelper
    {
        public static string MD5Encryption(string text)
        {
            StringBuilder sb = new StringBuilder();
            using (var md5 = MD5.Create())
            {
                var hash = md5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(text));
                foreach (byte ba in hash)
                {
                    sb.Append(ba.ToString("x2").ToLower());
                }
            }

            return sb.ToString();
        }
    }
}
