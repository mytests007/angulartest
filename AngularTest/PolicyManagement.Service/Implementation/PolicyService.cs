﻿using AutoMapper;
using PolicyManagement.Contracts;
using PolicyManagement.Model.Data;
using PolicyManagement.Model.ViewModel;
using PolicyManagement.Service.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyManagement.Service
{
    public class PolicyService : IPolicyService
    {
        private readonly IPolicyRepository _policyRepository;
        private readonly IMapper _mapper;

        public PolicyService(IPolicyRepository policyRepository, IMapper mapper)
        {
            _policyRepository = policyRepository;
            _mapper = mapper;
        }

        public async Task<int?> AddAsync(PolicyViewModel newPolicy) => 
            await _policyRepository.AddAsync(newPolicy.PolicyNumber, _mapper.Map<UserModel>(newPolicy));

        public async Task<CollectionResponse<PolicyViewModel>> GetAsync(FilterViewModel filterModel)
        {
            var policies = await _policyRepository.GetAsync();

            policies = policies.OrderByDescending(x => x.Updated);

            if (!string.IsNullOrWhiteSpace(filterModel.Filter))
            {
                policies = policies.Where(x => x.PolicyNumber.Contains(filterModel.Filter) || x.PolicyHolder.Name.CaseInsensitiveContains(filterModel.Filter, StringComparison.InvariantCultureIgnoreCase));
            }

            return new CollectionResponse<PolicyViewModel>
            {
                Total = policies.Count(),
                Items = _mapper.Map<List<PolicyViewModel>>(CollectionHelper.Paginate(filterModel, policies))
            }; 
        }

        public async Task<PolicyViewModel> GetAsync(int id) =>
            _mapper.Map<PolicyViewModel>(await _policyRepository.GetAsync(id));

        public async Task<bool?> RemoveAsync(int id) => await _policyRepository.RemoveAsync(id);

        public async Task<bool?> UpdateAsync(int id, PolicyViewModel policyVM) => 
            await _policyRepository.UpdateAsync(id, _mapper.Map<PolicyModel>(policyVM));
    }
}
