﻿using PolicyManagement.Contracts;
using PolicyManagement.Service.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PolicyManagement.Service.Implementation
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<bool> AuthenticateUser(string userName, string password)
        {
            var user = await _userRepository.GetByUserNameAsync(userName);
            var encryptedPassword = EncrptionHelper.MD5Encryption(user.Password);
            if (user != null && encryptedPassword.Equals(password.Trim(), StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            return false;
        }
    }
}
