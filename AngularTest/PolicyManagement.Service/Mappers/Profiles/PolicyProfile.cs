﻿using AutoMapper;
using PolicyManagement.Model.Data;
using PolicyManagement.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Service.Mappers.Profiles
{
    public class PolicyProfile : Profile
    {
        public PolicyProfile()
        {
            CreateMap<PolicyModel, PolicyViewModel>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.PolicyNumber, opts => opts.MapFrom(src => src.PolicyNumber))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.PolicyHolder.Name))
                .ForMember(dest => dest.Age, opts => opts.MapFrom(src => src.PolicyHolder.Age))
                .ForMember(dest => dest.Gender, opts => opts.MapFrom(src => src.PolicyHolder.Gender));

            CreateMap<PolicyViewModel, PolicyModel>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.PolicyNumber, opts => opts.MapFrom(src => src.PolicyNumber))
                .ForMember(dest => dest.PolicyHolder, opts => opts.MapFrom(src => src))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<PolicyViewModel, UserModel>()
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Age, opts => opts.MapFrom(src => src.Age))
                .ForMember(dest => dest.Gender, opts => opts.MapFrom(src => src.Gender))
                .ForAllOtherMembers(opts => opts.Ignore());
        }
    }
}
