﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using PolicyManagement.Controllers;
using PolicyManagement.Model.ViewModel;
using PolicyManagement.Service;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PolicyManagement.Tests.Controllers
{
    public class PolicyControllerTests
    {
        private readonly Mock<IPolicyService> _mockedPolicyService;
        private readonly PolicyController _testClassInstance;

        public PolicyControllerTests()
        {
            _mockedPolicyService = new Mock<IPolicyService>();
            _testClassInstance = new PolicyController(_mockedPolicyService.Object);
        }

        [Fact]
        public async Task GetAllPolicy_WhenNoDataPresent_NotFound()
        {
            _mockedPolicyService.Setup(x => x.GetAsync(It.IsAny<FilterViewModel>())).ReturnsAsync((CollectionResponse<PolicyViewModel>)null);

            var actual = await _testClassInstance.Get(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int?>());
            var result = actual as NotFoundResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(404);
        }

        [Fact]
        public async Task GetAllPolicy_WhenDataPresent_Ok()
        {
            _mockedPolicyService.Setup(x => x.GetAsync(It.IsAny<FilterViewModel>())).ReturnsAsync(new CollectionResponse<PolicyViewModel>());

            var actual = await _testClassInstance.Get(It.IsAny<string>(), It.IsAny<int?>(), It.IsAny<int?>());
            var result = actual as OkObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(200);
        }

        [Fact]
        public async Task GetPolicy_WhenNullPassed_BadRequest()
        {
            var actual = await _testClassInstance.Get(null);
            var result = actual as BadRequestObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(400);
        }

        [Fact]
        public async Task GetPolicy_WhenNoDataPresent_NotFound()
        {
            _mockedPolicyService.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync((PolicyViewModel)null);

            var actual = await _testClassInstance.Get(It.IsAny<int>());
            var result = actual as NotFoundResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(404);
        }

        [Fact]
        public async Task GetPolicy_WhenDataPresent_Ok()
        {
            _mockedPolicyService.Setup(x => x.GetAsync(It.IsAny<int>())).ReturnsAsync(new PolicyViewModel());

            var actual = await _testClassInstance.Get(It.IsAny<int>());
            var result = actual as OkObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(200);
        }

        [Fact]
        public async Task AddPolicy_WhenNullPassed_BadRequest()
        {
            var actual = await _testClassInstance.Post(null);
            var result = actual as BadRequestObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(400);
        }

        [Fact]
        public async Task AddPolicy_WhenNoDataPresent_StatusCode()
        {
            _mockedPolicyService.Setup(x => x.AddAsync(It.IsAny<PolicyViewModel>())).ReturnsAsync((int?)null);

            var actual = await _testClassInstance.Post(new PolicyViewModel());
            var result = actual as ObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(422);
        }

        [Fact]
        public async Task AddPolicy_WhenDataPresent_Ok()
        {
            _mockedPolicyService.Setup(x => x.AddAsync(It.IsAny<PolicyViewModel>())).ReturnsAsync(1);

            var actual = await _testClassInstance.Post(new PolicyViewModel());
            var result = actual as OkObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(200);
        }

        [Fact]
        public async Task UpdatePolicy_WhenIdPassedNull_BadRequest()
        {
            var actual = await _testClassInstance.Put(null, new PolicyViewModel());
            var result = actual as BadRequestObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(400);
        }

        [Fact]
        public async Task UpdatePolicy_WhenModelPassedNull_BadRequest()
        {
            var actual = await _testClassInstance.Put(1, null);
            var result = actual as BadRequestObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(400);
        }

        [Fact]
        public async Task UpdatePolicy_WhenNoDataPresent_NotFound()
        {
            _mockedPolicyService.Setup(x => x.UpdateAsync(It.IsAny<int>(), It.IsAny<PolicyViewModel>())).ReturnsAsync((bool?)null);

            var actual = await _testClassInstance.Put(1, new PolicyViewModel());
            var result = actual as NotFoundResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(404);
        }

        [Fact]
        public async Task UpdatePolicy_WhenDataPresent_Ok()
        {
            _mockedPolicyService.Setup(x => x.UpdateAsync(It.IsAny<int>(), It.IsAny<PolicyViewModel>())).ReturnsAsync(true);

            var actual = await _testClassInstance.Put(1, new PolicyViewModel());
            var result = actual as OkObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(200);
        }

        [Fact]
        public async Task DeletePolicy_WhenNullPassed_BadRequest()
        {
            var actual = await _testClassInstance.Delete(null);
            var result = actual as BadRequestObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(400);
        }

        [Fact]
        public async Task DeletePolicy_WhenNoDataPresent_NotFound()
        {
            _mockedPolicyService.Setup(x => x.RemoveAsync(It.IsAny<int>())).ReturnsAsync((bool?)null);

            var actual = await _testClassInstance.Delete(1);
            var result = actual as NotFoundResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(404);
        }

        [Fact]
        public async Task DeletePolicy_WhenDataPresent_Ok()
        {
            _mockedPolicyService.Setup(x => x.RemoveAsync(It.IsAny<int>())).ReturnsAsync(true);

            var actual = await _testClassInstance.Delete(1);
            var result = actual as OkObjectResult;

            result.ShouldNotBeNull();
            result.StatusCode.ShouldBe(200);
        }
    }
}
