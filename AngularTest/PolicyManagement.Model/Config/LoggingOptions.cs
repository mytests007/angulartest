﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Config
{
    public class LoggingOptions
    {
        public int LogLevel { get; set; }

        public string FilePath { get; set; }
    }
}
