﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Config
{
    public class TokenOptions
    {
        public string Audience { get; set; }

        public string Issuer { get; set; }

        public int AccessTokenExpiration { get; set; }

        public string KeyName { get; set; }
    }
}
