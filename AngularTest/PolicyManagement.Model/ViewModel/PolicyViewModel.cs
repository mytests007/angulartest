﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.ViewModel
{
    public class PolicyViewModel
    {
        public int Id { get; set; }
        public string PolicyNumber { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }
    }
}
