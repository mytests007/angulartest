﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.ViewModel
{
    public class FilterViewModel
    {
        public FilterViewModel(string filter, int? pageSize, int? pageNumber)
        {
            Filter = filter;
            PageSize = pageSize;
            PageNumber = pageNumber;
        }

        public string Filter { get; set; }

        public int? PageSize { get; set; }

        public int? PageNumber { get; set; }
    }
}
