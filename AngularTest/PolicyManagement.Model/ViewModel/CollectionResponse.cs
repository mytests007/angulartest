﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.ViewModel
{
    public class CollectionResponse<T>
    {
        public int Total { get; set; }

        public List<T> Items { get; set; }
    }
}
