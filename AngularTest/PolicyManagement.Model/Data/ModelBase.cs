﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Data
{
    public class ModelBase
    {
        public int Id { get; set; }

        public DateTime Updated { get; set; } = DateTime.UtcNow;
    }
}
