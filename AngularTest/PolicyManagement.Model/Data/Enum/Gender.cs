﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Data.Enum
{
    public enum Gender
    {
        Male,
        Female
    }
}
