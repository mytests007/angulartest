﻿using PolicyManagement.Model.Data.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Data
{
    public class UserModel: ModelBase
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string UserName { get; set; } = string.Empty;
        public string Password { get; set; }
        public Gender Gender { get; set; }
    }
}
