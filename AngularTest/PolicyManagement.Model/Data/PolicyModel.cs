﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Data
{
    public class PolicyModel: ModelBase
    {
        public PolicyModel()
        {
            this.PolicyHolder = new UserModel();
        }

        public PolicyModel(string policyNumber, UserModel policyHolder)
        {
            this.PolicyNumber = policyNumber;
            this.PolicyHolder = policyHolder;
            this.PolicyHolderId = policyHolder.Id;
        }

        public string PolicyNumber { get; set; }
        public int PolicyHolderId { get; set; }
        public UserModel PolicyHolder { get; set; }
    }
}
