﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PolicyManagement.Model.Logging
{
    public enum LogLevel
    {
        Off = 0,
        Verbose = 1,
        Info = 2,
        Warn = 3,
        Error = 4
    }
}
