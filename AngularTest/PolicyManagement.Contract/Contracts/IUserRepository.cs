﻿using System.Threading.Tasks;
using PolicyManagement.Model.Data;

namespace PolicyManagement.Contracts
{
    public interface IUserRepository
    {
        Task<int?> Add(UserModel user);

        Task<bool?> Update(int Id, UserModel user);

        Task<UserModel> GetByUserNameAsync(string userName);
    }
}