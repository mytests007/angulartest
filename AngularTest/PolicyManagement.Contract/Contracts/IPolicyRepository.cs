using PolicyManagement.Model.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyManagement.Contracts
{
    public interface IPolicyRepository
    {
        Task<IQueryable<PolicyModel>> GetAsync();

        Task<int?> AddAsync(string policyNumber, UserModel user);

        Task<bool?> UpdateAsync(int id, PolicyModel policy);

        Task<bool?> RemoveAsync(int id);

        Task<PolicyModel> GetAsync(int id);
    }
}