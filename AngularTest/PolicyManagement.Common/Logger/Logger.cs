﻿using Microsoft.Extensions.Options;
using PolicyManagement.Model.Config;
using PolicyManagement.Model.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PolicyManagement.Common.Logger
{
    public class Logger : ILogger
    {
        private readonly LoggingOptions _loggingOptions;

        public Logger(IOptions<LoggingOptions> loggingOptions)
        {
            _loggingOptions = loggingOptions.Value;
        }

        public void Log(string message, LogLevel logLevel, Exception exception = null)
        {
            if((_loggingOptions.LogLevel <= (int)logLevel) || logLevel == LogLevel.Error)
            {
                using (StreamWriter w = File.AppendText($"{_loggingOptions.FilePath}/log_{DateTime.Now.ToString("ddMMyyyy")}"))
                {
                    Log(message, w, exception);
                }
            }
        }

        private void Log(string message, StreamWriter w, Exception exception = null)
        {
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine($"  :{message}");

            if (exception != null)
            {
                w.WriteLine($"  :{exception.GetBaseException()}");
            }
        }
    }
}
