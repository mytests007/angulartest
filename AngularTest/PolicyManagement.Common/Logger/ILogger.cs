﻿using System;
using PolicyManagement.Model.Logging;

namespace PolicyManagement.Common.Logger
{
    public interface ILogger
    {
        void Log(string message, LogLevel logLevel, Exception exception = null);
    }
}