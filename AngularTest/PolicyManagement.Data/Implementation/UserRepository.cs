﻿using PolicyManagement.Contracts;
using PolicyManagement.Data.Context;
using PolicyManagement.Model.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PolicyManagement.Data.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly IDbContext _dbContext;

        public UserRepository(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int?> Add(UserModel user)
        {
            if (user == null)
            {
                return null;
            }

            return await _dbContext.Users.Add(user);
        }

        public async Task<bool?> Update(int Id, UserModel user)
        {
            var obj = await _dbContext.Users.Find(Id);
            if (obj == null || user == null)
            {
                return null;
            }

            obj.Age = user.Age;
            obj.Gender = user.Gender;
            obj.Name = user.Name;
            obj.Updated = DateTime.UtcNow;

            return true;
        }

        public async Task<UserModel> GetByUserNameAsync(string userName) =>
            (await _dbContext.Users.All()).SingleOrDefault(x => x.UserName.Equals(userName.Trim(), StringComparison.InvariantCultureIgnoreCase));
    }
}
