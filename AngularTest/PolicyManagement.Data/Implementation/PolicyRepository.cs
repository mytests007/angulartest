﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PolicyManagement.Contracts;
using PolicyManagement.Data.Context;
using PolicyManagement.Model.Data;

namespace PolicyManagement.Data.Implementation
{
    public class PolicyRepository : IPolicyRepository
    {
        private readonly IUserRepository _userRepository;
        private readonly IDbContext _dbContext;

        public PolicyRepository(IUserRepository userRepository, IDbContext dbContext)
        {
            _userRepository = userRepository;
            _dbContext = dbContext;
        }

        public async Task<int?> AddAsync(string policyNumber, UserModel user)
        {
            var userId = await _userRepository.Add(user);

            if(!userId.HasValue || userId == 0)
            {
                return null;
            }

            user.Id = userId.Value;
            var policy = new PolicyModel (policyNumber, user);
            return await _dbContext.Policies.Add(policy);
        }

        public async Task<bool?> RemoveAsync(int id)
        {
            var policy = await _dbContext.Policies.Find(id);
            if(policy == null)
            {
                return null;
            }

            var userAssigned = (await _dbContext.Policies.All()).Where(x => x.PolicyHolderId == policy.PolicyHolderId && x.Id != id).Any();
            if(userAssigned || await _dbContext.Users.Remove(policy.PolicyHolderId))
            {
                return await _dbContext.Policies.Remove(id);
            }

            return false;
        }

        public async Task<bool?> UpdateAsync(int id, PolicyModel policy)
        {
            var obj = await _dbContext.Policies.Find(id);
            if (obj == null || policy == null)
            {
                return null;
            }

            obj.PolicyNumber = policy.PolicyNumber;
            obj.Updated = DateTime.UtcNow;
            return await _userRepository.Update(obj.PolicyHolderId, policy.PolicyHolder);
        }

        public async Task<IQueryable<PolicyModel>> GetAsync() => await _dbContext.Policies.All();

        public async Task<PolicyModel> GetAsync(int Id) => await _dbContext.Policies.Find(Id);
    }
}
