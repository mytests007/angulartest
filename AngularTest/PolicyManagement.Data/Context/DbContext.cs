﻿using PolicyManagement.Model.Data;
using PolicyManagement.Model.Data.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolicyManagement.Data.Context
{
    public class DbContext : IDbContext
    {
        public DbContext()
        {
            this.Policies = new DbSet<PolicyModel>();
            this.Users = new DbSet<UserModel>();
            Initialise();
        }

        public DbSet<PolicyModel> Policies { get; set; }

        public DbSet<UserModel> Users { get; set; }

        #region Set Initial Data
        private async Task Initialise()
        {
            await Users.Add(new UserModel
            {
                Id = 103,
                Name = "Admin",
                UserName = "Admin",
                Password = "Password!1",
                Age = 42,
                Gender = Gender.Female
            });

            await Users.Add(new UserModel
            {
                Id = 100,
                Name = "Dwayne Johnson",
                Age = 44,
                Gender = Gender.Male
            });

            await Users.Add(new UserModel
            {
                Id = 101,
                Name = "John Cena",
                Age = 38,
                Gender = Gender.Male
            });

            await Users.Add(new UserModel
            {
                Id = 102,
                Name = "Trish Stratus",
                Age = 42,
                Gender = Gender.Female
            });

            var policy = 462947;
            for (var i = 0; i < 20; i++)
            {
                await Policies.Add(new PolicyModel
                {
                    PolicyNumber = policy.ToString(),
                    PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("John Cena")).Id,
                    PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("John Cena"))
                });

                policy++;
            }

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "739562",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Dwayne Johnson")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Dwayne Johnson"))
            });

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "383002",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Dwayne Johnson")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Dwayne Johnson"))
            });

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "462946",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("John Cena")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("John Cena"))
            });

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "355679",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus"))
            });

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "589881",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus"))
            });

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "998256",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus"))
            });

            await Policies.Add(new PolicyModel
            {
                PolicyNumber = "100374",
                PolicyHolderId = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus")).Id,
                PolicyHolder = (await Users.All()).SingleOrDefault(x => x.Name.Equals("Trish Stratus"))
            });

        }
        #endregion
    }
}
