﻿using System.Linq;
using System.Collections.Generic;
using System.Text;
using PolicyManagement.Model.Data;
using System.Threading.Tasks;

namespace PolicyManagement.Data.Context
{
    public class DbSet<T> where T : ModelBase
    {
        private List<T> _content = new List<T>();
        private int _index = 0;

        public async Task<int> Add(T entry)
        {
            _index++;
            entry.Id = _index;
            await Task.Run(() => _content.Add(entry));
            
            return entry.Id;
        }

        public async Task<bool> Remove(int Id)
        {
            var obj = _content.SingleOrDefault(c => c.Id == Id);
            await Task.Run(() => _content.Remove(obj));

            return true;
        }

        public async Task<IQueryable<T>> All() => await Task.Run(() => _content.AsQueryable());

        public async Task<T> Find(int Id) => await Task.Run(() => _content.SingleOrDefault(x => x.Id == Id));
    }
}
