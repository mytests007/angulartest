﻿using System.Collections.Generic;
using PolicyManagement.Model.Data;

namespace PolicyManagement.Data.Context
{
    public interface IDbContext
    {
        DbSet<PolicyModel> Policies { get; set; }
        DbSet<UserModel> Users { get; set; }
    }
}