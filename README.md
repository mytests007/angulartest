Please use following credentials to login to the application.

username: Admin

password: Password!1


Description about project
---------------------------
-> Created custom token authentication to improve security the API's.

-> Hased password when senting to backend API.

-> Crated server side paging and filter for better performace of application.

-> Used a static in memory list to store policies.

-> Designed the solution in such a way that data sore can be easily replaced with any other data storage/ database.

-> Logged the exception to a local file inside log folder. This also can be eaily replaces by implemented ILog interface.

-> Xunit used to add unit test for backend

-> Jasmine used to add unit test for front end

